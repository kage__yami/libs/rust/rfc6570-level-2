use anyhow::Context;

test_cases! {
    component::varname: {
        case_address: "address",
        case_base: "base",
        case_count: "count",
        case_dom: "dom",
        case_dub: "dub",
        case_email: "email_address",
        case_empty: "empty",
        case_empty_keys: "empty_keys",
        case_half: "half",
        case_hello: "hello",
        case_keys: "keys",
        case_lang: "lang",
        case_list: "list",
        case_name: "name.first",
        case_number: "number",
        case_path: "path",
        case_q: "q",
        case_query: "query",
        case_semi: "semi",
        case_source: "source%E2%87%92destination",
        case_term: "term",
        case_undef: "undef",
        case_username: "username",
        case_v: "v",
        case_var: "var",
        case_who: "who",
        case_x: "x",
        case_y: "y",
        case_year: "year"
    }
}

# RFC6570 Level 2 - Rust <!-- omit in toc -->

- [Overview](#overview)
  - [Versioning](#versioning)
  - [Repository information](#repository-information)
- [Usage](#usage)

## Overview

A Rust library for validating and processing strings as RFC6570-compliant URIs (up to level 2).

[![docs.io documentation](https://img.shields.io/badge/docs.io-Documentation-orange?style=for-the-badge)](https://docs.rs/rfc6570-level-2)
[![crates.io version](https://img.shields.io/crates/v/rfc6570-level-2?style=for-the-badge)](https://docs.rs/rfc6570-level-2)
[![crates.io downloads](https://img.shields.io/crates/d/rfc6570-level-2?style=for-the-badge)](https://crates.io/crates/rfc6570-level-2)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/Kage-Yami/rfc6570-level-2-rust/main?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc6570-level-2-rust/pipelines/main/latest)
[![Gitlab code coverage](https://img.shields.io/gitlab/coverage/Kage-Yami/rfc6570-level-2-rust/main?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc6570-level-2-rust)
[![Lines of code](https://img.shields.io/tokei/lines/gitlab/Kage-Yami/rfc6570-level-2-rust?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc6570-level-2-rust)
[![Dependents](https://img.shields.io/librariesio/dependent-repos/cargo/rfc6570-level-2?style=for-the-badge)](https://libraries.io/cargo/rfc6570-level-2)
[![License](https://img.shields.io/crates/l/rfc6570-level-2?style=for-the-badge)](https://gitlab.com/Kage-Yami/rfc6570-level-2-rust/-/blob/main/LICENSE)

### Versioning

This project follows [Semantic Versioning principals](https://semver.org/) starting with `v1.0.0`

### Repository information

This repository is located on [GitLab.com](https://gitlab.com/Kage-Yami/rfc6570-level-2-rust).

## Usage

To use this library, instantiate an `UriTemplate` with a relevant string. From here, the result can be "discarded" if only validation of the input string is needed, or a list of contained expressions or variables can be retrieved with `expressions()` or `variables()`. Finally, the URI template can be expanded by called `expand()` with a `HashMap<&str, &str>` of variables and values.

```rust
use rfc6570_level_2::UriTemplate;

let template = UriTemplate::new("https://example.com/{resource}/{+id}{#field}")?;

// What variables are available?
let variables: Vec<&str> = template.variables().collect();
assert_eq!(variables, vec!["resource", "id", "field"]);

let var_map = [
    ("resource", "user"),
    ("id", "5"),
    ("field", "email"),
].iter().cloned().collect();

// Expand the template
let uri = template.expand(&var_map);
assert_eq!(uri, "https://example.com/user/5#email");
```
